package com.rangsiman.midterm;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    static Map map = new Map(20, 20);
    static Player Player = new Player(map, 'A', 10, 10);
    static Obstacle Obstacle1 = new Obstacle(map, 5, 5);
    static Obstacle Obstacle2 = new Obstacle(map, 5, 6);
    static Obstacle Obstacle3 = new Obstacle(map, 6, 7);
    static Obstacle Obstacle4 = new Obstacle(map, 16, 5);
    static Obstacle Obstacle5 = new Obstacle(map, 7, 15);
    static Obstacle Obstacle6 = new Obstacle(map, 4, 2);
    static Obstacle Obstacle7 = new Obstacle(map, 4, 13);
    static Obstacle Obstacle8 = new Obstacle(map, 10, 7);
    static Obstacle Obstacle9 = new Obstacle(map, 5, 10);
    static Obstacle Obstacle10 = new Obstacle(map, 10, 6);
    static Obstacle Obstacle11 = new Obstacle(map, 9, 7);
    static Obstacle Obstacle12 = new Obstacle(map, 19, 17);
    static Obstacle Obstacle13 = new Obstacle(map, 15, 15);
    static Obstacle Obstacle14 = new Obstacle(map, 15, 16);
    static Obstacle Obstacle15 = new Obstacle(map, 15, 17);
    static Obstacle Obstacle16 = new Obstacle(map, 16, 15);
    static Obstacle Obstacle17 = new Obstacle(map, 17, 15);
    static Obstacle Obstacle18 = new Obstacle(map, 14, 12);
    static Obstacle Obstacle19 = new Obstacle(map, 4, 13);
    static Obstacle Obstacle20 = new Obstacle(map, 6, 17);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "w":
                Player.up();
                break;
            case "s":
                Player.down();
                break;
            case "a":
                Player.left();
                break;
            case "d":
                Player.right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        map.add(Obstacle1);
        map.add(Obstacle2);
        map.add(Obstacle3);
        map.add(Obstacle4);
        map.add(Obstacle5);
        map.add(Obstacle6);
        map.add(Obstacle7);
        map.add(Obstacle8);
        map.add(Obstacle9);
        map.add(Obstacle10);
        map.add(Obstacle11);
        map.add(Obstacle12);
        map.add(Obstacle13);
        map.add(Obstacle14);
        map.add(Obstacle15);
        map.add(Obstacle16);
        map.add(Obstacle17);
        map.add(Obstacle18);
        map.add(Obstacle19);
        map.add(Obstacle20);
        map.add(Obstacle11);
        map.add(Player);

        while (true) {
            map.print();
            String command = input();
            process(command);
        }
    }
}
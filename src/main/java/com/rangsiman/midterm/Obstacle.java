package com.rangsiman.midterm;

public class Obstacle extends Unit {

    public Obstacle(Map map, int x, int y) {
        super(map, 'X', x, y, true);
    }
    
}
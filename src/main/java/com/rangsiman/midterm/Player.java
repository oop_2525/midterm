package com.rangsiman.midterm;

public class Player extends Unit {

    public Player(Map map, char symbol, int x, int y) {
        super(map, symbol, x, y, false);
    }
    
    public void up() {
        int y = this.getY();
        y--;
        this.setY(y);
    }

    public void down() {
        int y = this.getY();
        y++;
        this.setY(y);
    }

    public void left() {
        double x = this.getX();
        x--;
        this.setX(x);
    }

    public void right() {
        double x = this.getX();
        x++;
        this.setX(x);
    }
}


package com.rangsiman.midterm;

public class Unit {
    private double x;
    private int y;
    private char symbol;
    private Map map;
    private boolean dominate;

    public Unit(Map map, char symbol, double x, int y, boolean dominate) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
        this.dominate = dominate;
    }

    public boolean isOn(double x2, int y) {
        return this.x == x2 && this.y == y;
    }

    public boolean setX(double x2) {
        if(!map.isOn(x2, y)) {
            return false;
        }
        if(map.hasDominate(x2, y)) {
            System.out.println("## YOU LOSE ##");
            System.exit(0);
        }
        this.x = x2;
        return true;
    } 

    public boolean setY(int y) {
        if(!map.isOn(x, y)) {
            return false;
        }
        if(map.hasDominate(x, y)) {
            System.out.println("## YOU LOSE ##");
            System.exit(0);
        }
        this.y = y;
        return true;
    }

    public boolean setXY(int x, int y) {
        if(!map.isOn(x, y)) {
            return false;
        }
        if(map.hasDominate(x, y)) {
            System.out.println("## YOU LOSE ##");
            System.exit(0);
        }
        this.x = x;
        this.y = y;
        return true;
    }

    public double getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isDominate() {
        return dominate;
    }

    public Map getMap() {
        return map;
    }

    public char getSymbol() {
        return symbol;
    }

    public String toString() {
        return "Unit(" + this.symbol + ") [" + this.x + "," + this.y + "] is on " + map;
    }
}

